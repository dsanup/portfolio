-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 03, 2019 at 10:15 AM
-- Server version: 10.2.23-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wapssolution_personalmanagement`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_waps`
--

CREATE TABLE `admin_waps` (
  `id` bigint(20) NOT NULL,
  `name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `email` varchar(150) CHARACTER SET utf8 NOT NULL,
  `password` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `register` datetime NOT NULL,
  `type` tinyint(11) NOT NULL DEFAULT 1,
  `image` varchar(255) NOT NULL,
  `permission` int(11) NOT NULL DEFAULT 0,
  `lastlogin` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `about` text NOT NULL,
  `education` text NOT NULL,
  `designation` varchar(30) NOT NULL,
  `location` varchar(50) NOT NULL,
  `templink` varchar(100) NOT NULL,
  `requesttime` varchar(50) NOT NULL,
  `validtime` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_waps`
--

INSERT INTO `admin_waps` (`id`, `name`, `email`, `password`, `status`, `register`, `type`, `image`, `permission`, `lastlogin`, `about`, `education`, `designation`, `location`, `templink`, `requesttime`, `validtime`) VALUES
(1, 'Anup Mondal', 'anup12.m@gmail.com', '3859766560f16da9272879dc90ac29dc', 1, '2018-05-15 23:41:52', 3, 'c87475f9d67dd41fa61a5eee8f618779.jpg', 1, '2019-01-15 18:30:28', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n', 'Senior Programmer', 'Dhaka, Bangladesh', '8ceb1f52c309549499f19e40a4cb2eb0', '2019-01-16 12:30:28am', '2019-01-18 12:30:28am'),
(2, 'Piklu', 'admin@admin.com', '827ccb0eea8a706c4c34a16891f84e7b', 1, '0000-00-00 00:00:00', 1, '', 0, '2018-05-18 15:50:57', '', '', '', '', '', '0000-00-00 00:00:00', ''),
(3, 'hello', 'admin2@admin.com', 'd41d8cd98f00b204e9800998ecf8427e', 0, '0000-00-00 00:00:00', 2, '', 0, '2018-11-11 18:12:41', '', '', '', '', '', '0000-00-00 00:00:00', ''),
(4, 'sagor', 'sagor@admin.com', '1234', 1, '0000-00-00 00:00:00', 0, '', 0, '2018-05-18 15:42:44', '', '', '', '', '', '0000-00-00 00:00:00', ''),
(7, 'Palas', 'palas@admin.com', 'ec6a6536ca304edf844d1d248a4f08dc', 1, '0000-00-00 00:00:00', 2, '', 0, '2018-05-18 16:13:46', '', '', '', '', '', '0000-00-00 00:00:00', ''),
(8, 'Binoy', 'binoy@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 1, '0000-00-00 00:00:00', 1, '', 0, '2018-12-08 19:53:15', '', '', '', '', '', '0000-00-00 00:00:00', ''),
(9, 'palash sarker', 'palashsarker855@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 1, '0000-00-00 00:00:00', 1, '', 0, '2018-12-11 15:25:43', '', '', '', '', '', '0000-00-00 00:00:00', ''),
(10, 'vhum', 'vhumi@gmail.com', '25d55ad283aa400af464c76d713c07ad', 1, '0000-00-00 00:00:00', 1, '', 0, '2018-12-11 17:33:10', '', '', '', '', '', '0000-00-00 00:00:00', '');

-- --------------------------------------------------------

--
-- Table structure for table `balance`
--

CREATE TABLE `balance` (
  `id` int(11) NOT NULL,
  `expenseTotal` int(11) NOT NULL,
  `earnTotal` int(11) NOT NULL,
  `userId` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `balance`
--

INSERT INTO `balance` (`id`, `expenseTotal`, `earnTotal`, `userId`) VALUES
(1, 65381, 1334, 1),
(2, 0, 0, 10),
(3, 5050, 24100, 9);

-- --------------------------------------------------------

--
-- Table structure for table `borrow`
--

CREATE TABLE `borrow` (
  `id` bigint(20) NOT NULL,
  `borrowFrom` varchar(255) NOT NULL,
  `bDate` date NOT NULL,
  `bComments` text NOT NULL,
  `bdatetime` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `borrowAmount` int(11) NOT NULL,
  `borrowBalance` int(11) NOT NULL,
  `userID` bigint(20) NOT NULL,
  `borrowGive` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `borrow`
--

INSERT INTO `borrow` (`id`, `borrowFrom`, `bDate`, `bComments`, `bdatetime`, `status`, `borrowAmount`, `borrowBalance`, `userID`, `borrowGive`) VALUES
(1, 'Uttam Kumar ', '2018-10-10', '<p>long time</p>\r\n\r\n<p>* 2017 puja</p>\r\n\r\n<p>* drinks purpose</p>\r\n', '2018-12-17 13:52:50', 0, 3500, 3500, 1, 0),
(2, 'Chayan', '2018-12-16', '<p>* Puja Eat 2018</p>\r\n\r\n<p>* Drinks (17-12)</p>\r\n', '2019-03-20 16:18:27', 0, 1600, 500, 1, 1100),
(3, 'Palash Basa', '2018-12-04', '<p>*</p>\r\n', '2018-12-18 19:11:17', 0, 1000, 0, 1, 1000),
(5, 'Sanjoy Da', '2018-12-14', '', '2018-12-17 15:34:36', 0, 200, 200, 9, 0),
(6, 'Sanjoy ', '2018-12-18', '', '2018-12-18 19:11:01', 0, 100, 0, 1, 100),
(7, 'Sanjoy Da', '2018-12-25', '', '2019-01-11 15:02:56', 0, 100, 0, 1, 100),
(8, 'Palash', '2018-12-26', '', '2019-01-16 14:27:42', 0, 1000, 0, 1, 1000),
(9, 'Palash', '2019-01-23', '<p>* Tour&nbsp;</p>\r\n', '2019-03-10 17:23:13', 0, 5000, 0, 1, 5000),
(10, 'Bikash', '2019-03-21', '<p>pay to next month</p>\r\n', '2019-04-13 17:02:31', 0, 3000, 0, 1, 3000),
(11, 'Sanjoy', '2019-03-31', '', '2019-05-02 14:08:04', 0, 1000, 0, 1, 1000);

-- --------------------------------------------------------

--
-- Table structure for table `borrowlendhistory`
--

CREATE TABLE `borrowlendhistory` (
  `id` bigint(20) NOT NULL,
  `borrowId` bigint(20) NOT NULL,
  `lendId` bigint(20) NOT NULL,
  `hamount` int(11) NOT NULL,
  `hdate` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `userId` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `borrowlendhistory`
--

INSERT INTO `borrowlendhistory` (`id`, `borrowId`, `lendId`, `hamount`, `hdate`, `userId`) VALUES
(2, 0, 2, 46, '2018-12-12 08:00:00', 1),
(3, 4, 0, 50, '2018-12-17 08:00:00', 1),
(4, 0, 6, 100, '2018-12-17 08:00:00', 9),
(5, 6, 0, 100, '2018-12-18 08:00:00', 1),
(6, 3, 0, 1000, '2018-12-18 08:00:00', 1),
(7, 2, 0, 100, '2018-12-16 08:00:00', 1),
(8, 7, 0, 100, '2019-01-11 07:00:00', 1),
(9, 8, 0, 1000, '2019-01-16 07:00:00', 1),
(10, 0, 3, 50, '2019-01-21 07:00:00', 1),
(11, 0, 9, 500, '2019-01-30 07:00:00', 1),
(12, 9, 0, 2000, '2019-02-06 07:00:00', 1),
(13, 0, 8, 200, '2019-02-08 07:00:00', 1),
(14, 9, 0, 3000, '2019-03-10 07:00:00', 1),
(15, 2, 0, 1000, '2019-03-20 07:00:00', 1),
(16, 0, 10, 1000, '2019-03-21 07:00:00', 1),
(17, 10, 0, 3000, '2019-04-13 07:00:00', 1),
(18, 11, 0, 1000, '2019-05-02 07:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `diary`
--

CREATE TABLE `diary` (
  `id` bigint(20) NOT NULL,
  `dDate` date NOT NULL,
  `dTitle` varchar(150) NOT NULL,
  `dBrief` text NOT NULL,
  `userId` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `earn`
--

CREATE TABLE `earn` (
  `id` bigint(20) NOT NULL,
  `eDate` date NOT NULL,
  `purId` int(11) NOT NULL,
  `earnPurpose` varchar(150) NOT NULL,
  `eAmount` int(20) NOT NULL,
  `eTime` varchar(20) NOT NULL,
  `userId` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `earn`
--

INSERT INTO `earn` (`id`, `eDate`, `purId`, `earnPurpose`, `eAmount`, `eTime`, `userId`) VALUES
(1, '2018-12-11', 1, '', 4000, '12:04 am', 1),
(2, '2018-12-12', 1, '', 3000, '12:19 am', 1),
(3, '2018-12-17', 1, '', 100, '07:46 pm', 1),
(4, '2018-12-10', 0, 'Revenue', 22000, '09:27 pm', 9),
(5, '2018-12-25', 0, 'Lunch facilities', 2000, '09:38 pm', 9),
(6, '2018-12-18', 1, '', 3000, '01:10 am', 1),
(8, '2018-12-27', 0, 'Adv. Salary', 500, '08:38 pm', 1),
(10, '2018-12-30', 0, 'From Home', 1000, '07:24 pm', 1),
(11, '2019-01-09', 0, 'Adv. Salary', 100, '12:09 am', 1),
(12, '2019-01-10', 1, '', 5000, '07:19 pm', 1),
(13, '2019-01-15', 1, '', 5000, '10:40 pm', 1),
(14, '2019-02-01', 0, 'Mama', 500, '06:55 pm', 1),
(15, '2019-02-01', 0, 'Home', 1000, '06:55 pm', 1),
(16, '2019-02-05', 1, '', 5000, '12:15 am', 1),
(17, '2019-02-18', 1, '', 5000, '10:41 pm', 1),
(18, '2019-03-10', 1, '', 15000, '11:23 pm', 1),
(20, '2019-04-05', 0, 'Home', 1000, '11:36 pm', 1),
(21, '2019-04-11', 0, 'Home', 5000, '09:03 pm', 1),
(22, '2019-04-12', 1, '', 5000, '08:30 pm', 1),
(23, '2019-04-26', 1, '', 4000, '09:16 pm', 1);

-- --------------------------------------------------------

--
-- Table structure for table `expense`
--

CREATE TABLE `expense` (
  `id` bigint(20) NOT NULL,
  `exDate` date NOT NULL,
  `exPurpose` varchar(150) NOT NULL,
  `exAmount` int(20) NOT NULL,
  `exTime` varchar(20) NOT NULL,
  `purId` bigint(20) NOT NULL,
  `userId` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `expense`
--

INSERT INTO `expense` (`id`, `exDate`, `exPurpose`, `exAmount`, `exTime`, `purId`, `userId`) VALUES
(1, '2018-12-10', '', 3169, '08:21 pm', 2, 1),
(2, '2018-12-10', 'Mill Due', 218, '08:20 pm', 0, 1),
(3, '2018-12-09', 'Light For Fish', 75, '12:08 am', 0, 1),
(4, '2018-12-10', 'Borrow Sanjoy Da', 100, '12:13 am', 0, 1),
(5, '2018-12-11', '', 50, '08:21 pm', 3, 1),
(6, '2018-12-11', '', 62, '08:23 pm', 4, 1),
(7, '2018-12-11', '', 25, '08:23 pm', 5, 1),
(10, '2018-12-12', '', 140, '12:27 am', 3, 1),
(11, '2018-12-12', '', 104, '12:31 am', 4, 1),
(12, '2018-12-12', 'Fruits buy for family', 250, '12:31 am', 0, 1),
(13, '2018-12-13', '', 29, '12:35 am', 8, 1),
(14, '2018-12-13', 'Nitto marriage', 1020, '06:43 pm', 0, 1),
(15, '2018-12-14', '', 156, '12:05 am', 4, 1),
(16, '2018-12-14', '', 140, '08:12 pm', 3, 1),
(17, '2018-12-15', '', 50, '12:04 am', 9, 1),
(18, '2018-12-14', '', 75, '12:05 am', 8, 1),
(19, '2018-12-14', '', 306, '12:06 am', 10, 1),
(20, '2018-12-15', '', 45, '07:29 pm', 3, 1),
(21, '2018-12-15', '', 90, '11:13 pm', 4, 1),
(22, '2018-12-15', 'Sampo', 10, '07:30 pm', 0, 1),
(24, '2018-12-15', '', 173, '11:14 pm', 10, 1),
(25, '2018-12-16', '', 398, '07:51 pm', 10, 1),
(26, '2018-12-16', '', 120, '07:40 pm', 4, 1),
(27, '2018-12-16', '', 40, '07:41 pm', 3, 1),
(28, '2018-12-16', 'T-Shirt Buy', 100, '07:42 pm', 0, 1),
(29, '2018-12-17', '', 50, '07:45 pm', 3, 1),
(30, '2018-12-17', '', 58, '07:45 pm', 4, 1),
(31, '2018-12-12', '', 50, '09:21 pm', 11, 9),
(32, '2018-12-31', '', 3000, '09:51 pm', 7, 9),
(33, '2018-12-31', 'Mill Exp.', 2000, '09:51 pm', 0, 9),
(34, '2018-12-18', '', 45, '01:08 am', 3, 1),
(35, '2018-12-18', '', 37, '01:09 am', 4, 1),
(36, '2018-12-18', 'cookies', 70, '01:10 am', 0, 1),
(37, '2017-10-09', 'Puja', 1500, '01:12 am', 0, 1),
(38, '2018-09-04', 'Drinks Uttam', 2000, '01:12 am', 0, 1),
(39, '2018-12-04', 'Multi Purpose', 1000, '01:13 am', 0, 1),
(40, '2018-12-16', 'Drinks Chayan', 1100, '01:14 am', 0, 1),
(41, '2018-10-24', 'Puja Eat, Chayan', 500, '01:14 am', 0, 1),
(44, '2018-12-19', '', 45, '10:59 pm', 3, 1),
(45, '2018-12-19', '', 36, '10:59 pm', 4, 1),
(46, '2018-12-20', '', 35, '11:51 pm', 3, 1),
(47, '2018-12-20', '', 40, '11:52 pm', 4, 1),
(48, '2018-12-20', 'Green Tea', 100, '11:52 pm', 0, 1),
(49, '2018-12-21', '', 30, '09:53 pm', 3, 1),
(50, '2018-12-21', '', 532, '11:41 pm', 10, 1),
(51, '2018-12-21', '', 111, '11:40 pm', 4, 1),
(52, '2018-12-21', 'Buy Angel Fish', 80, '09:55 pm', 0, 1),
(53, '2018-12-22', '', 103, '11:42 pm', 4, 1),
(54, '2018-12-22', '', 10, '11:42 pm', 9, 1),
(55, '2018-12-23', '', 45, '08:14 pm', 3, 1),
(56, '2018-12-23', '', 55, '09:22 am', 4, 1),
(57, '2018-12-24', '', 45, '10:36 pm', 4, 1),
(58, '2018-12-24', '', 37, '10:37 pm', 4, 1),
(59, '2018-12-25', '', 40, '06:44 pm', 3, 1),
(60, '2018-12-25', '', 19, '06:44 pm', 4, 1),
(61, '2018-12-25', 'Sukumar Mobile Recharge', 24, '10:53 pm', 0, 1),
(62, '2018-12-26', '', 50, '11:28 pm', 3, 1),
(63, '2018-12-26', '', 35, '11:28 pm', 4, 1),
(64, '2018-12-26', 'Hair Cut', 110, '11:29 pm', 0, 1),
(65, '2018-12-27', '', 155, '08:34 pm', 3, 1),
(66, '2018-12-27', 'Fruit Buy', 300, '08:35 pm', 0, 1),
(67, '2018-12-27', '', 100, '08:35 pm', 8, 1),
(68, '2018-12-28', '', 30, '08:36 pm', 8, 1),
(69, '2018-12-28', '', 45, '08:36 pm', 4, 1),
(70, '2018-12-28', 'Feather for polapain', 80, '08:37 pm', 0, 1),
(71, '2018-12-29', '', 95, '07:18 pm', 4, 1),
(72, '2018-12-31', '', 210, '07:25 pm', 3, 1),
(73, '2018-12-31', '', 47, '11:13 pm', 4, 1),
(74, '2018-12-30', 'Kalta election', 300, '07:28 pm', 0, 1),
(75, '2019-01-01', '', 65, '08:51 pm', 3, 1),
(76, '2019-01-01', '', 43, '10:26 pm', 9, 1),
(77, '2019-01-01', '', 22, '10:26 pm', 4, 1),
(78, '2019-01-02', '', 50, '10:29 pm', 3, 1),
(79, '2019-01-02', '', 30, '10:30 pm', 4, 1),
(80, '2019-01-02', 'Rin Powder', 25, '10:30 pm', 0, 1),
(81, '2019-01-03', '', 45, '10:31 pm', 3, 1),
(82, '2019-01-03', '', 20, '10:31 pm', 4, 1),
(83, '2019-01-04', '', 20, '09:42 pm', 4, 1),
(84, '2019-01-04', '', 30, '09:42 pm', 8, 1),
(85, '2019-01-05', '', 50, '07:06 pm', 3, 1),
(86, '2019-01-05', '', 45, '07:06 pm', 4, 1),
(87, '2019-01-05', 'Sampo', 10, '07:06 pm', 0, 1),
(88, '2019-01-05', 'Blade', 5, '07:06 pm', 0, 1),
(89, '2019-01-06', '', 45, '10:14 pm', 3, 1),
(90, '2019-01-06', '', 14, '10:14 pm', 4, 1),
(91, '2019-01-07', '', 50, '10:24 pm', 3, 1),
(92, '2019-01-07', '', 86, '10:26 pm', 4, 1),
(93, '2019-01-08', '', 23, '11:15 pm', 4, 1),
(94, '2019-01-08', '', 50, '11:15 pm', 3, 1),
(95, '2019-01-09', '', 25, '12:10 am', 4, 1),
(96, '2019-01-09', '', 50, '12:10 am', 3, 1),
(97, '2019-01-10', '', 45, '07:20 pm', 3, 1),
(98, '2019-01-10', '', 53, '07:24 pm', 4, 1),
(99, '2019-01-10', '', 3010, '11:39 pm', 2, 1),
(100, '2019-01-11', '', 730, '08:31 pm', 10, 1),
(101, '2019-01-11', '', 85, '09:00 pm', 3, 1),
(102, '2019-01-11', 'Book Buy ( Fluent Python)', 250, '09:01 pm', 0, 1),
(103, '2019-01-11', 'God Pic. Buy', 220, '09:01 pm', 0, 1),
(104, '2019-01-11', 'Prosad Eat', 75, '09:02 pm', 0, 1),
(105, '2019-01-11', 'Mobile Repair', 40, '09:04 pm', 0, 1),
(106, '2019-01-11', '', 20, '09:04 pm', 4, 1),
(107, '2019-01-11', '', 25, '09:08 pm', 9, 1),
(108, '2019-01-12', '', 50, '11:24 pm', 3, 1),
(109, '2019-01-12', '', 44, '11:24 pm', 8, 1),
(110, '2019-01-12', '', 47, '11:24 pm', 4, 1),
(111, '2019-01-13', '', 50, '07:23 pm', 3, 1),
(112, '2019-01-13', '', 50, '07:23 pm', 4, 1),
(113, '2019-01-14', '', 40, '12:07 am', 3, 1),
(114, '2019-01-14', '', 38, '12:07 am', 4, 1),
(115, '2019-01-15', '', 50, '10:41 pm', 3, 1),
(116, '2019-01-15', '', 105, '10:41 pm', 4, 1),
(117, '2019-01-16', '', 35, '08:26 pm', 3, 1),
(118, '2019-01-16', '', 280, '08:26 pm', 9, 1),
(119, '2019-01-16', '', 40, '08:27 pm', 4, 1),
(120, '2019-01-17', '', 50, '11:54 pm', 3, 1),
(121, '2019-01-17', '', 28, '11:55 pm', 4, 1),
(122, '2019-01-17', 'Agarobati', 10, '11:55 pm', 0, 1),
(123, '2019-01-17', 'Sampo', 12, '11:55 pm', 0, 1),
(124, '2019-01-18', '', 780, '10:47 pm', 10, 1),
(125, '2019-01-18', '', 30, '10:47 pm', 4, 1),
(126, '2019-01-19', '', 40, '10:47 pm', 3, 1),
(127, '2019-01-19', 'Trade Fair', 180, '10:48 pm', 0, 1),
(128, '2019-01-19', '', 35, '10:48 pm', 4, 1),
(129, '2019-01-20', '', 44, '08:31 pm', 8, 1),
(130, '2019-01-20', '', 60, '08:33 pm', 3, 1),
(131, '2019-01-20', '', 37, '08:33 pm', 4, 1),
(132, '2019-01-21', '', 50, '08:34 pm', 3, 1),
(133, '2019-01-21', '', 43, '08:34 pm', 4, 1),
(134, '2019-01-21', 'Shoe Shelai', 50, '08:34 pm', 0, 1),
(135, '2019-01-21', '', 35, '08:35 pm', 9, 1),
(136, '2019-01-21', 'Light For Table Lamp', 30, '02:02 am', 0, 1),
(137, '2019-01-22', '', 50, '07:44 pm', 3, 1),
(138, '2019-01-22', '', 30, '07:44 pm', 4, 1),
(139, '2019-01-22', 'Brush', 40, '07:44 pm', 0, 1),
(140, '2019-01-22', 'Biscuit', 15, '07:45 pm', 0, 1),
(141, '2019-01-23', '', 50, '11:19 pm', 3, 1),
(142, '2019-01-23', '', 71, '11:20 pm', 4, 1),
(143, '2019-01-23', 'Mujja', 25, '11:20 pm', 0, 1),
(144, '2019-01-24', '', 65, '05:28 pm', 3, 1),
(145, '2019-01-24', '', 24, '05:29 pm', 4, 1),
(146, '2019-01-24', '', 10, '05:30 pm', 12, 1),
(147, '2019-01-24', 'Hair Cut', 50, '05:31 pm', 0, 1),
(148, '2019-01-24', 'Buy T-shirt', 110, '05:31 pm', 0, 1),
(149, '2019-01-24', '', 50, '10:56 pm', 3, 1),
(150, '2019-01-24', '', 48, '10:57 pm', 4, 1),
(151, '2019-01-24', '', 110, '11:01 pm', 8, 1),
(152, '2019-01-24', '', 25, '11:31 pm', 9, 1),
(153, '2019-01-27', 'Tour Purpose (24-27 Land)', 5500, '06:14 pm', 0, 1),
(154, '2019-01-27', '', 50, '12:50 am', 4, 1),
(155, '2019-01-27', 'Agarobati', 10, '12:50 am', 0, 1),
(156, '2019-01-28', '', 135, '11:59 pm', 3, 1),
(157, '2019-01-28', 'Biscuit', 10, '11:59 pm', 0, 1),
(158, '2019-01-29', 'Surf Excel', 25, '12:00 am', 0, 1),
(159, '2019-01-29', '', 15, '12:00 am', 4, 1),
(160, '2019-01-29', '', 45, '01:36 am', 3, 1),
(161, '2019-01-29', '', 44, '01:37 am', 0, 1),
(162, '2019-01-30', '', 45, '10:26 pm', 3, 1),
(163, '2019-01-30', '', 43, '10:27 pm', 4, 1),
(164, '2019-01-31', '', 37, '10:30 pm', 3, 1),
(165, '2019-01-31', '', 55, '10:32 pm', 4, 1),
(166, '2019-02-01', '', 80, '10:53 pm', 3, 1),
(167, '2019-02-01', '', 28, '10:54 pm', 4, 1),
(168, '2019-01-31', '', 355, '06:56 pm', 10, 1),
(169, '2019-02-01', '', 50, '06:57 pm', 9, 1),
(170, '2019-02-02', '', 15, '11:50 pm', 8, 1),
(171, '2019-02-02', '', 10, '11:50 pm', 4, 1),
(172, '2019-02-03', '', 100, '12:50 am', 3, 1),
(173, '2019-02-03', '', 10, '12:50 am', 4, 1),
(174, '2019-02-04', '', 45, '11:00 pm', 8, 1),
(175, '2019-02-04', '', 58, '11:00 pm', 4, 1),
(176, '2019-02-04', 'Sugar', 10, '11:00 pm', 0, 1),
(177, '2019-02-04', 'Agarobati', 10, '11:00 pm', 0, 1),
(178, '2019-02-05', 'all', 100, '12:15 am', 0, 1),
(179, '2019-02-06', 'Total', 45, '09:59 pm', 0, 1),
(180, '2019-02-07', '', 80, '10:52 am', 9, 1),
(181, '2019-02-07', '', 30, '10:52 am', 4, 1),
(182, '2019-02-08', 'Sharoswati Puja', 500, '10:54 am', 0, 1),
(183, '2019-02-09', 'Puja', 1500, '05:55 pm', 0, 1),
(184, '2019-02-16', 'Home (8-17)', 850, '05:57 pm', 0, 1),
(185, '2019-02-17', '', 304, '08:35 pm', 10, 1),
(186, '2019-02-17', '', 70, '08:36 pm', 8, 1),
(187, '2019-02-17', '', 40, '08:36 pm', 4, 1),
(188, '2019-02-17', 'Cotton bar', 10, '08:37 pm', 0, 1),
(189, '2019-02-17', 'Extra', 50, '08:38 pm', 0, 1),
(191, '2019-02-18', '', 3317, '10:40 pm', 2, 1),
(192, '2019-02-18', '', 45, '10:42 pm', 3, 1),
(193, '2019-02-18', '', 20, '10:43 pm', 4, 1),
(194, '2019-02-19', '', 50, '10:49 am', 3, 1),
(195, '2019-02-19', '', 53, '10:50 am', 4, 1),
(196, '2019-03-01', '', 60, '08:42 pm', 3, 1),
(197, '2019-02-28', 'Total Multiple Purpose', 1800, '11:22 pm', 0, 1),
(198, '2019-03-10', '', 3500, '11:26 pm', 2, 1),
(200, '2019-03-10', 'Bikash Back', 1500, '11:37 pm', 0, 1),
(202, '2019-03-10', 'Sanjoy Back', 200, '11:48 pm', 0, 1),
(204, '2019-03-01', '', 394, '11:50 pm', 10, 1),
(205, '2019-03-11', 'Agarbati + Rin + Sugar', 45, '08:08 pm', 0, 1),
(206, '2019-03-11', '', 75, '08:11 pm', 4, 1),
(207, '2019-03-11', 'swapan mama load', 36, '08:09 pm', 0, 1),
(208, '2019-03-11', '', 50, '08:10 pm', 3, 1),
(209, '2019-03-12', 'Watch Belt', 120, '08:09 pm', 0, 1),
(210, '2019-03-12', 'Underwear', 150, '08:09 pm', 0, 1),
(211, '2019-03-12', '', 140, '08:09 pm', 8, 1),
(212, '2019-03-12', '', 70, '08:10 pm', 3, 1),
(213, '2019-03-12', '', 25, '08:10 pm', 9, 1),
(214, '2019-03-12', '', 60, '08:10 pm', 4, 1),
(215, '2019-03-13', 'Mill Due', 50, '04:34 pm', 0, 1),
(216, '2019-03-15', 'Marriage of Niko', 1000, '12:14 am', 0, 1),
(217, '2019-03-15', 'Home Fruit', 200, '12:15 am', 0, 1),
(218, '2019-03-15', 'Home Visit', 520, '12:17 am', 0, 1),
(219, '2019-03-15', '', 40, '12:11 am', 3, 1),
(220, '2019-03-15', '', 30, '12:11 am', 4, 1),
(221, '2019-03-15', '', 288, '12:11 am', 10, 1),
(222, '2019-03-16', 'Fan Repair', 600, '09:59 pm', 0, 1),
(223, '2019-03-16', '', 50, '10:00 pm', 3, 1),
(224, '2019-03-16', '', 37, '10:00 pm', 4, 1),
(225, '2019-03-16', 'T-Bag', 50, '10:00 pm', 0, 1),
(226, '2019-03-17', '', 60, '08:37 pm', 4, 1),
(227, '2019-03-17', 'Father Mobile Recharge', 42, '11:03 pm', 0, 1),
(228, '2019-03-18', '', 50, '10:30 pm', 3, 1),
(229, '2019-03-18', '', 75, '10:30 pm', 4, 1),
(230, '2019-03-19', '', 50, '11:06 pm', 4, 1),
(231, '2019-03-20', '', 45, '10:17 pm', 3, 1),
(232, '2019-03-20', '', 62, '10:17 pm', 4, 1),
(233, '2019-03-20', '', 32, '10:17 pm', 5, 1),
(234, '2019-03-20', 'Extra', 20, '10:17 pm', 0, 1),
(235, '2019-03-21', '', 45, '02:27 pm', 3, 1),
(236, '2019-03-21', '', 50, '02:28 pm', 4, 1),
(237, '2019-03-22', '', 145, '02:30 pm', 3, 1),
(238, '2019-03-22', '', 16, '02:30 pm', 4, 1),
(239, '2019-03-20', 'Extra', 10, '02:32 pm', 0, 1),
(240, '2019-03-22', '', 42, '08:34 pm', 4, 1),
(241, '2019-03-22', '', 30, '08:34 pm', 8, 1),
(242, '2019-03-22', '', 373, '08:34 pm', 10, 1),
(243, '2019-03-23', '', 50, '08:35 pm', 3, 1),
(244, '2019-03-23', '', 52, '08:35 pm', 4, 1),
(245, '2019-03-23', '', 10, '08:37 pm', 12, 1),
(246, '2019-03-24', '', 45, '10:38 pm', 3, 1),
(247, '2019-03-24', '', 76, '10:39 pm', 4, 1),
(248, '2019-03-25', 'T-shirt', 240, '07:41 pm', 0, 1),
(249, '2019-03-25', '', 50, '07:42 pm', 3, 1),
(250, '2019-03-25', '', 100, '02:21 pm', 4, 1),
(251, '2019-03-25', 'Blade', 5, '07:42 pm', 0, 1),
(252, '2019-03-25', '', 30, '07:43 pm', 8, 1),
(253, '2019-03-26', '', 86, '04:38 pm', 4, 1),
(254, '2019-03-26', 'Domain - Hosting', 2000, '04:37 pm', 0, 1),
(255, '2019-03-26', '', 85, '04:38 pm', 3, 1),
(256, '2019-03-27', '', 80, '10:57 pm', 3, 1),
(258, '2019-03-26', '', 25, '04:40 pm', 9, 1),
(259, '2019-03-26', 'Print docs', 100, '04:42 pm', 0, 1),
(260, '2019-03-27', 'Charger', 60, '10:58 pm', 0, 1),
(261, '2019-03-27', '', 20, '10:59 pm', 4, 1),
(262, '2019-03-28', '', 50, '07:04 pm', 3, 1),
(263, '2019-03-28', '', 147, '11:59 pm', 4, 1),
(264, '2019-03-29', 'Agarobati', 10, '11:59 pm', 0, 1),
(265, '2019-03-29', '', 478, '02:07 pm', 10, 1),
(266, '2019-03-29', '', 76, '02:07 pm', 4, 1),
(267, '2019-03-31', '', 28, '02:08 pm', 4, 1),
(268, '2019-03-31', '', 50, '02:08 pm', 3, 1),
(269, '2019-03-31', 'Unknown', 30, '02:10 pm', 0, 1),
(270, '2019-03-31', '', 42, '10:31 pm', 4, 1),
(271, '2019-04-01', '', 30, '10:31 pm', 9, 1),
(272, '2019-04-01', '', 55, '10:32 pm', 4, 1),
(273, '2019-04-01', '', 30, '10:32 pm', 8, 1),
(274, '2019-04-02', '', 50, '10:58 pm', 4, 1),
(275, '2019-04-02', 'Photocopy', 70, '10:58 pm', 0, 1),
(276, '2019-04-03', '', 35, '12:10 am', 4, 1),
(277, '2019-04-03', '', 40, '12:12 am', 3, 1),
(278, '2019-04-04', '', 40, '02:02 am', 3, 1),
(279, '2019-04-04', '', 16, '02:02 am', 5, 1),
(280, '2019-04-04', 'Agarobati', 10, '02:02 am', 0, 1),
(281, '2019-04-04', '', 66, '02:02 am', 4, 1),
(282, '2019-04-04', 'Money Bag', 50, '02:06 am', 0, 1),
(283, '2019-04-05', '', 143, '11:36 pm', 0, 1),
(284, '2019-04-05', '', 10, '11:37 pm', 12, 1),
(285, '2019-04-05', '', 45, '11:38 pm', 3, 1),
(286, '2019-04-06', '', 40, '10:26 pm', 3, 1),
(287, '2019-04-06', '', 33, '10:26 pm', 4, 1),
(288, '2019-04-06', '', 395, '11:12 pm', 10, 1),
(289, '2019-04-07', '', 40, '11:13 pm', 3, 1),
(290, '2019-04-07', '', 35, '11:13 pm', 4, 1),
(291, '2019-04-08', '', 40, '08:42 pm', 3, 1),
(292, '2019-04-08', '', 20, '08:42 pm', 4, 1),
(293, '2019-04-09', '', 45, '08:42 pm', 3, 1),
(294, '2019-04-09', '', 20, '08:42 pm', 4, 1),
(295, '2019-04-09', 'Sugar + Rin + Agarbati', 50, '08:43 pm', 0, 1),
(296, '2019-04-10', '', 24, '11:28 pm', 5, 1),
(297, '2019-04-10', '', 25, '11:28 pm', 4, 1),
(298, '2019-04-10', '', 45, '11:28 pm', 3, 1),
(299, '2019-04-11', '', 3634, '09:03 pm', 2, 1),
(300, '2019-04-11', '', 50, '09:04 pm', 3, 1),
(301, '2019-04-11', '', 20, '09:04 pm', 4, 1),
(302, '2019-04-11', 'Toast', 60, '09:04 pm', 0, 1),
(303, '2019-04-11', '', 110, '09:04 pm', 8, 1),
(304, '2019-04-11', 'Gas Light', 10, '08:26 pm', 0, 1),
(305, '2019-04-12', '', 404, '08:31 pm', 10, 1),
(306, '2019-04-12', '', 60, '08:31 pm', 9, 1),
(307, '2019-04-12', '', 20, '08:31 pm', 4, 1),
(308, '2019-04-13', '', 50, '11:01 pm', 3, 1),
(309, '2019-04-13', '', 30, '11:01 pm', 8, 1),
(310, '2019-04-13', '', 20, '11:01 pm', 4, 1),
(311, '2019-04-14', '', 100, '09:20 pm', 4, 1),
(312, '2019-04-14', '', 85, '09:21 pm', 3, 1),
(313, '2019-04-14', 'Fish Equipment', 80, '09:22 pm', 0, 1),
(314, '2019-04-15', '', 45, '08:12 pm', 3, 1),
(315, '2019-04-15', '', 12, '08:44 pm', 4, 1),
(316, '2019-04-16', '', 45, '10:20 pm', 3, 1),
(317, '2019-04-16', '', 32, '10:20 pm', 4, 1),
(318, '2019-04-16', 'Anju Mobile ', 30, '10:21 pm', 0, 1),
(319, '2019-04-17', '', 45, '08:35 pm', 3, 1),
(320, '2019-04-17', '', 12, '08:35 pm', 4, 1),
(321, '2019-04-17', '', 16, '08:35 pm', 5, 1),
(322, '2019-04-18', '', 65, '11:28 pm', 3, 1),
(323, '2019-04-18', '', 63, '11:28 pm', 4, 1),
(324, '2019-04-18', 'Drinks', 181, '11:30 pm', 0, 1),
(325, '2019-04-19', '', 84, '11:54 pm', 4, 1),
(326, '2019-04-19', '', 363, '10:22 pm', 10, 1),
(327, '2019-04-20', '', 40, '10:23 pm', 3, 1),
(328, '2019-04-20', '', 35, '10:23 pm', 4, 1),
(329, '2019-04-20', 'Agarobati + Sampo', 20, '10:23 pm', 0, 1),
(330, '2019-04-21', '', 40, '10:37 pm', 3, 1),
(331, '2019-04-21', '', 230, '10:37 pm', 9, 1),
(332, '2019-04-21', '', 40, '10:38 pm', 4, 1),
(333, '2019-04-21', 'Others', 5, '10:38 pm', 0, 1),
(334, '2019-04-22', '', 42, '10:33 pm', 4, 1),
(335, '2019-04-22', 'Shoe Repair', 50, '10:33 pm', 0, 1),
(336, '2019-04-23', '', 12, '10:30 pm', 4, 1),
(337, '2019-04-23', '', 45, '10:30 pm', 3, 1),
(338, '2019-04-24', '', 140, '08:28 pm', 8, 1),
(339, '2019-04-24', '', 45, '08:28 pm', 3, 1),
(340, '2019-04-24', '', 30, '08:29 pm', 4, 1),
(341, '2019-04-25', '', 45, '10:42 pm', 3, 1),
(342, '2019-04-25', '', 24, '10:42 pm', 5, 1),
(343, '2019-04-26', '', 140, '09:14 pm', 3, 1),
(344, '2019-04-26', '', 20, '09:14 pm', 4, 1),
(345, '2019-04-27', '', 45, '09:14 pm', 4, 1),
(346, '2019-04-28', '', 130, '09:15 pm', 3, 1),
(347, '2019-04-28', '', 55, '09:15 pm', 4, 1),
(348, '2019-04-28', 'Body Spray', 300, '09:15 pm', 0, 1),
(349, '2019-04-28', '', 10, '09:16 pm', 8, 1),
(350, '2019-04-28', 'Unknown', 50, '09:17 pm', 0, 1),
(351, '2019-04-28', 'Dinar', 70, '11:17 pm', 0, 1),
(352, '2019-04-29', '', 45, '01:56 am', 3, 1),
(353, '2019-04-29', 'Hair cutting', 120, '01:58 am', 0, 1),
(354, '2019-04-29', 'Breakfast', 42, '01:59 am', 0, 1),
(355, '2019-04-30', 'Snan', 20, '02:01 am', 0, 1),
(356, '2019-04-30', '', 40, '11:25 pm', 3, 1),
(357, '2019-04-30', '', 57, '11:25 pm', 4, 1),
(358, '2019-04-30', 'Snan', 20, '11:25 pm', 0, 1),
(359, '2019-04-30', 'Fan Repair', 50, '11:25 pm', 0, 1),
(360, '2019-05-01', '', 20, '11:04 pm', 4, 1),
(361, '2019-05-01', 'Snan', 30, '11:04 pm', 0, 1),
(362, '2019-05-01', 'Visit to Atik House', 470, '11:06 pm', 0, 1),
(363, '2019-05-02', '', 50, '08:10 pm', 4, 1),
(364, '2019-05-02', '', 60, '08:10 pm', 9, 1),
(365, '2019-05-03', '', 45, '06:22 pm', 3, 1),
(366, '2019-05-03', 'Breakfast', 37, '06:22 pm', 0, 1),
(367, '2019-05-03', '', 522, '08:07 pm', 10, 1),
(368, '2019-05-03', 'Agarobati', 10, '06:53 pm', 0, 1),
(369, '2019-05-03', '', 20, '06:53 pm', 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `lend`
--

CREATE TABLE `lend` (
  `id` bigint(20) NOT NULL,
  `lendTo` varchar(255) NOT NULL,
  `lDate` date NOT NULL,
  `lComment` text NOT NULL,
  `ldatetime` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `lendAmount` int(11) NOT NULL,
  `lendBalance` int(11) NOT NULL,
  `userID` bigint(20) NOT NULL,
  `lendReturn` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `lend`
--

INSERT INTO `lend` (`id`, `lendTo`, `lDate`, `lComment`, `ldatetime`, `status`, `lendAmount`, `lendBalance`, `userID`, `lendReturn`) VALUES
(2, 'Bijoy', '2018-12-12', '<p>Office</p>\r\n', '2018-12-12 18:24:10', 0, 100, 54, 1, 46),
(3, 'Mithu', '2018-12-15', '', '2019-01-21 20:01:37', 0, 50, 0, 1, 50),
(4, 'Partho Da', '2018-11-23', '', '2018-12-17 15:32:25', 0, 500, 500, 9, 0),
(5, 'Anup Da', '2018-12-11', '', '2018-12-17 15:33:08', 0, 1000, 1000, 9, 0),
(6, 'Susanto Da', '2018-12-15', '', '2018-12-17 16:04:57', 0, 100, 0, 9, 100),
(7, 'Piklu', '2018-12-19', '<p>*will return 4-5 day&#39;s latter</p>\r\n', '2018-12-19 17:01:09', 0, 510, 510, 1, 0),
(8, 'Aslam', '2018-12-31', '<p>Car Rent</p>\r\n', '2019-02-08 04:53:15', 0, 200, 0, 1, 200),
(9, 'Parvez', '2019-01-16', '<p>should be return next week.</p>\r\n', '2019-01-30 16:28:03', 0, 500, 0, 1, 500),
(10, 'Bikash', '2019-03-18', '<p>Return check</p>\r\n', '2019-03-22 08:28:46', 0, 1100, 100, 1, 1000);

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `message` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `purpose`
--

CREATE TABLE `purpose` (
  `id` bigint(20) NOT NULL,
  `purpose` varchar(255) NOT NULL,
  `type` varchar(70) NOT NULL,
  `userId` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `purpose`
--

INSERT INTO `purpose` (`id`, `purpose`, `type`, `userId`) VALUES
(1, 'Salary', 'Earn Purpose', 1),
(2, 'House Rent', 'Expense Purpose', 1),
(3, 'Car Rent', 'Expense Purpose', 1),
(4, 'Tea ', 'Expense Purpose', 1),
(5, 'Laundry', 'Expense Purpose', 1),
(6, 'Salary', 'Earn Purpose', 9),
(7, 'House Rent', 'Expense Purpose', 9),
(8, 'Mobile Recharge', 'Expense Purpose', 1),
(9, 'Medicine', 'Expense Purpose', 1),
(10, 'Mill Bazzar', 'Expense Purpose', 1),
(11, 'Entertainment', 'Expense Purpose', 9),
(12, 'Sampo', 'Expense Purpose', 1);

-- --------------------------------------------------------

--
-- Table structure for table `task`
--

CREATE TABLE `task` (
  `id` bigint(20) NOT NULL,
  `startDate` date NOT NULL,
  `endDate` date NOT NULL,
  `title` varchar(150) NOT NULL,
  `taskBreif` text NOT NULL,
  `emailStatus` tinyint(4) DEFAULT 0,
  `mobileStatus` tinyint(4) NOT NULL DEFAULT 0,
  `taskComplete` varchar(150) NOT NULL,
  `userId` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `task`
--

INSERT INTO `task` (`id`, `startDate`, `endDate`, `title`, `taskBreif`, `emailStatus`, `mobileStatus`, `taskComplete`, `userId`) VALUES
(1, '2018-12-11', '2018-12-31', 'Automate The Boring Stuff With Python', '<p>Complete the Book within this 2018.</p>\r\n\r\n<p>*The book is closed 11 Jan, 2019.</p>\r\n\r\n<p>* some excersize was not complete fully.</p>\r\n', 0, 0, 'Completed', 1),
(2, '2018-12-11', '0000-00-00', 'Using Facebook API', '<p>Using facebook for login any website.</p>\r\n', 0, 0, 'On Hold', 1),
(3, '2018-12-11', '0000-00-00', 'SSL service', '<p>SSL service for website secure.</p>\r\n\r\n<p>* Task done for main website and htaccess</p>\r\n\r\n<p>* Try to next time for subfolder and subdomain</p>\r\n\r\n<p>* Everything is done.&nbsp;</p>\r\n', 0, 0, 'Completed', 1),
(4, '2018-12-11', '0000-00-00', 'Automatic email and mobile message', '<p>Automatic email and mobile message by check time scheduling</p>\r\n\r\n<p>*Automatic email is done by cron job (18/01/2019)</p>\r\n\r\n<p>*&nbsp;curl --silent https://wapssolution.com/mymanagement/Admin_panel/corn_job</p>\r\n', 0, 0, 'On Hold', 1),
(5, '2018-12-11', '2018-12-18', 'Automate The Boring Stuff With Python', '<p>ddd</p>\r\n', 0, 0, 'To Do', 9),
(6, '2018-12-11', '0000-00-00', 'Hello', '', 0, 0, 'Completed', 9),
(7, '2018-12-12', '2018-12-20', 'MyManagement', '<p>To Find Debug and Solve and Update New Feature.</p>\r\n\r\n<p>* forget password email verification &amp; link create limited time. (task done)</p>\r\n\r\n<p>* need sticky note in index for today list</p>\r\n', 0, 0, 'On Hold', 1),
(8, '2018-12-12', '0000-00-00', 'Git Version', '<p>* What is GIT</p>\r\n\r\n<p>* how it&#39;s work&nbsp;</p>\r\n', 0, 0, 'Doing', 1),
(9, '2019-01-19', '2019-01-19', 'Phone Call List', '<p>1. Call to Sentu To know about him and his father - Hold, Munim vai, Sabuj for project.</p>\r\n', 0, 0, 'Doing', 1),
(10, '2018-12-14', '0000-00-00', 'Software & web application testing', '<p>Software &amp; web application testing for php and codeigniter, like php unit testing</p>\r\n', 0, 0, 'Doing', 1),
(11, '2018-12-15', '2018-12-16', 'Deshnet Bd', '<p>end tomorrow all requirement</p>\r\n\r\n<p>* now setup everything</p>\r\n', 0, 0, 'Completed', 1),
(13, '2019-01-19', '0000-00-00', 'Udemy Python', '<p>End to as soon as possible. try to this month.</p>\r\n', 0, 0, 'On Hold', 1);

-- --------------------------------------------------------

--
-- Table structure for table `wsxq_admin`
--

CREATE TABLE `wsxq_admin` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(32) NOT NULL,
  `u_name` varchar(50) DEFAULT NULL,
  `status` enum('0','1') NOT NULL,
  `registered` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `lastlogon` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `image` varchar(225) NOT NULL,
  `type` enum('5','10') NOT NULL,
  `permission` enum('0','1') NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wsxq_admin`
--

INSERT INTO `wsxq_admin` (`id`, `email`, `password`, `u_name`, `status`, `registered`, `lastlogon`, `image`, `type`, `permission`) VALUES
(28, 'mhsohel017@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', 'Morshed Habib', '1', '2016-11-06 01:12:16', '0000-00-00 00:00:00', '', '10', '1'),
(29, 'mhsohel018@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'sohel', '1', '2018-09-19 05:28:30', '0000-00-00 00:00:00', '', '10', '1'),
(31, 'anup12.m@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 'Anup Mondal', '1', '2018-09-03 17:23:05', '0000-00-00 00:00:00', '', '10', '1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_waps`
--
ALTER TABLE `admin_waps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `balance`
--
ALTER TABLE `balance`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userId` (`userId`);

--
-- Indexes for table `borrow`
--
ALTER TABLE `borrow`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userID` (`userID`);

--
-- Indexes for table `borrowlendhistory`
--
ALTER TABLE `borrowlendhistory`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userId` (`userId`);

--
-- Indexes for table `diary`
--
ALTER TABLE `diary`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userId` (`userId`);

--
-- Indexes for table `earn`
--
ALTER TABLE `earn`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `expense`
--
ALTER TABLE `expense`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userId` (`userId`);

--
-- Indexes for table `lend`
--
ALTER TABLE `lend`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userID` (`userID`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purpose`
--
ALTER TABLE `purpose`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userId` (`userId`);

--
-- Indexes for table `task`
--
ALTER TABLE `task`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userId` (`userId`);

--
-- Indexes for table `wsxq_admin`
--
ALTER TABLE `wsxq_admin`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_waps`
--
ALTER TABLE `admin_waps`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `balance`
--
ALTER TABLE `balance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `borrow`
--
ALTER TABLE `borrow`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `borrowlendhistory`
--
ALTER TABLE `borrowlendhistory`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `diary`
--
ALTER TABLE `diary`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `earn`
--
ALTER TABLE `earn`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `expense`
--
ALTER TABLE `expense`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=370;

--
-- AUTO_INCREMENT for table `lend`
--
ALTER TABLE `lend`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `purpose`
--
ALTER TABLE `purpose`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `task`
--
ALTER TABLE `task`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `wsxq_admin`
--
ALTER TABLE `wsxq_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `balance`
--
ALTER TABLE `balance`
  ADD CONSTRAINT `balance_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `admin_waps` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `borrow`
--
ALTER TABLE `borrow`
  ADD CONSTRAINT `borrow_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `admin_waps` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `borrowlendhistory`
--
ALTER TABLE `borrowlendhistory`
  ADD CONSTRAINT `borrowlendhistory_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `admin_waps` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `diary`
--
ALTER TABLE `diary`
  ADD CONSTRAINT `diary_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `admin_waps` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `expense`
--
ALTER TABLE `expense`
  ADD CONSTRAINT `expense_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `admin_waps` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `lend`
--
ALTER TABLE `lend`
  ADD CONSTRAINT `lend_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `admin_waps` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `purpose`
--
ALTER TABLE `purpose`
  ADD CONSTRAINT `purpose_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `admin_waps` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `task`
--
ALTER TABLE `task`
  ADD CONSTRAINT `task_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `admin_waps` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
